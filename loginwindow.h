#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QMainWindow>
#include <QHostAddress>

namespace Ui {
class LoginWindow;
}

class LoginWindow : public QMainWindow
{
    Q_OBJECT
private:
    Ui::LoginWindow *ui;

public:
    explicit LoginWindow(QWidget *parent = 0);
    ~LoginWindow();


public slots:
    void showMessageLoginFailed();


signals:
    void signalLogin(QHostAddress server, qint16 port, std::string, std::string);


private slots:
    void slotLoginClicked();

};

#endif // LOGINWINDOW_H
