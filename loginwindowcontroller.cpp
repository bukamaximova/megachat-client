#include "loginwindowcontroller.h"
#include <QObject>

LoginWindowController::LoginWindowController(ServerConnection *connection)
{
    m_hidden = true;
    m_connection = connection;

    connect(&m_window, &LoginWindow::signalLogin,
            this, &LoginWindowController::onLoginClicked);
}



void LoginWindowController::showWindow()
{
    m_window.show();
    m_hidden = false;
}



void LoginWindowController::hideWindow()
{
    m_window.hide();
    m_hidden = true;
}



void LoginWindowController::showLoginFailed()
{
    m_window.showMessageLoginFailed();
}




void LoginWindowController::onLoginClicked(QHostAddress server, qint16 port, std::string login, std::string password)
{
    qDebug() << "Connecting to " << server << ":" << port;

    if(login.empty() || password.empty()) {
        m_window.showMessageLoginFailed();
    }
    else {
        m_server_address = server;
        m_login = login;
        m_password = password;
        m_connection->connectToServer(server, port);
    }

}


void LoginWindowController::connectedToServer()
{
    qDebug() << "Connected. Trying login L:" << m_login.c_str() << " P:" << m_password.c_str();
    m_connection->login(m_login, m_password);
}

