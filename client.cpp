#include "client.h"

#include <QtDebug>

Client::Client(QApplication *app)
{
    m_qtApplication = app;

//    m_eventManager = new EventManager();

    m_serverConnection = new ServerConnection(this);

    m_loginWindowController = new LoginWindowController(m_serverConnection);
    m_mainWindow_Controller = new MainWindowController();
    m_chatWindowList = new ChatWindowList();

//    m_mainWindow_Controller->hideWindow();
    m_mainWindow_Controller->showWindow();
    m_loginWindowController->hideWindow();

    QObject::connect(m_loginWindowController, &LoginWindowController::quit,
                     [&]() {
                                 this->quit();
                           }
    );

    qDebug() << "Client created";
}


Client::~Client()
{
//    delete m_eventManager;
    delete m_serverConnection;

    delete m_loginWindowController;
    delete m_mainWindow_Controller;
    delete m_chatWindowList;

    qDebug() << "Client destroyed";
}


void Client::launch()
{
    qDebug() << "Client started!";

    m_loginWindowController->showWindow();

//    m_chatWindowList->showChat("petuh");
}



void Client::quit()
{
    qDebug() << "Client quits.";
    m_qtApplication->quit();
}



// Events from ServerConnection

void Client::onConnected()
{
    qDebug() << "Connected to server sending login";
    m_loginWindowController->connectedToServer();
}


void Client::onDisconnected()
{

    qDebug() << "Disconnected from server";
}


void Client::onLoginAck(char code)
{
    qDebug() << "Server response LOGIN_ACK with code " << (int)code;
    if(code == 0){
        // Close login, open main window
        m_loginWindowController->hideWindow();
        m_mainWindow_Controller->showWindow();
    }
    else {
        // Show error message
        m_loginWindowController->showLoginFailed();
    }
}


void Client::onLogoutAck()
{
    qDebug() << "Server response LOGOUT_ACK";
}


void Client::onContactStatusChanged(const std::string &username, char status)
{
    //    -> m_mainWindow_Controller::statusChanged(status) -> model::updateStatus() -> signal to Window
    m_mainWindow_Controller->statusChanged(username,status);
}

void Client::onMessageIncoming(const std::string &sender, int message_id, const std::string &message)
{
    qDebug() << "New messgae from " << sender.c_str() << " :(id " << message_id << ")" << message.c_str();
    m_chatWindowList->displayMessage(sender, message_id, message);
}


void Client::onMessageDelivered(int message_id, bool success)
{
    qDebug() << "Message Id: " << message_id << " was delivered: " << success;
    m_chatWindowList->messageDelivered(message_id, success);
}

/*
void Client::onContactStatusChanged(const std::string &username, char status)
{
    qDebug() << "Status changed. User: " << username.c_str() << " status: " << (int)status;
//    m_mainWindow_Controller->updateStatus(username, status);
}*/
