#ifndef SERVERCONNECTION_H
#define SERVERCONNECTION_H

#include <QtCore>
#include <QtNetwork>


#include <chatconnection.h>
#include <interfaces/ichatconnectionlistener.h>

#include "qtcpconnection.h"



class IServerConnectionListener
{
public:
    virtual ~IServerConnectionListener() {}

    virtual void onConnected() = 0;
    virtual void onDisconnected() = 0;
    virtual void onLoginAck(char code) = 0;
    virtual void onLogoutAck() = 0;

    virtual void onMessageIncoming(const std::string& sender, int message_id, const std::string& message) = 0;
    virtual void onMessageDelivered(int message_id, bool success) = 0;

    virtual void onContactStatusChanged(const std::string& username, char status) = 0;
};



class ServerConnection : public QObject, public IChatConnectionListener
{
    Q_OBJECT

private:
    bool m_is_opened;

    QTcpConnection * m_tcp_connection;
    ChatConnection * m_chat_connection;

    IServerConnectionListener * m_listener;

public:
    ServerConnection(IServerConnectionListener * listener);
    virtual ~ServerConnection();


    void connectToServer(const QHostAddress& server_address, qint16 port);
    void disconnectFromServer();

    void login(const std::string& username, const std::string& password);


    virtual void onOpened();
    virtual void onClosed();

    // For login controller
    virtual void onLoginAck(char code);
    virtual void onLogoutAck();
    // For chat window controller
    virtual void onMessageOutAck(int message_id, char code);
    virtual void onMessageIn(std::string sender, int message_id, std::string message);
    // For main window controller
    virtual void onContactListResponse(std::vector<User> contacts) {}
    virtual void onPresence(std::string username, u_int8_t status);

    //
    virtual void onKeepAlive() {}



    // Not used
    virtual void onKeepAliveAck() {}
    virtual void onPresenceAck(std::string) {}
    virtual void onContactListRequest() {}
    virtual void onMessageInAck(int, char) {}
    virtual void onMessageOut(std::string, int, std::string) {}
    virtual void onLogout() {}
    virtual void onLogin(std::string, std::string) {}
};



#endif // SERVERCONNECTION_H
