#ifndef LOGINWINDOWMODEL_H
#define LOGINWINDOWMODEL_H

#include <QObject>
#include <string>



class LoginWindowModel : public QObject
{
    Q_OBJECT
private:
    std::string m_username;
    std::string m_password;


public:
    LoginWindowModel(QObject *parent = 0);
    ~LoginWindowModel();


signals:
    void loginFailed();
    void loginSucceded();


public slots:
    void tryLogin(std::string username, std::string password);

};


#endif // LOGINWINDOWMODEL_H
