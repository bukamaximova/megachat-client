#include "mainwindowmodel.h"

MainWindowModel::MainWindowModel(QObject *parent): QObject(parent)
{}

MainWindowModel::~MainWindowModel()
{}


void MainWindowModel::clear()
{
    m_contacts.clear();
}


void MainWindowModel::addContact(std::string username, char status)
{
//    m_contacts[username] = status;
    m_contacts.insert(username, status);
}


void MainWindowModel::updateStatus(std::string username, char status)
{
    m_contacts[username] = status;
    emit signalUpdateContactList(&m_contacts);
}


const QMap<std::string, char> * MainWindowModel::getContacts() const
{
    return &m_contacts;
}
