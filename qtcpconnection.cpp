#include "qtcpconnection.h"

QTcpConnection::QTcpConnection(const QHostAddress& address, qint16 port, QObject *parent) : QObject(parent)
{
    m_socket = new QTcpSocket(this);
    m_remote_address = address;
    m_remote_port = port;
    m_listener = 0;

    connect(m_socket, &QTcpSocket::connected,
            this, &QTcpConnection::socketConnected);
    connect(m_socket, &QTcpSocket::disconnected,
            this, &QTcpConnection::socketDisconnected);
    connect(m_socket, &QTcpSocket::readyRead,
            this, &QTcpConnection::socketReadyRead);
}

QTcpConnection::~QTcpConnection()
{
    m_socket->disconnectFromHost();
    connect(m_socket, &QTcpSocket::disconnected,
            m_socket, &QTcpSocket::deleteLater);
}

void QTcpConnection::setListener(IByteConnectionListener *listener)
{
    m_listener = listener;
}



// IByteConnection

void QTcpConnection::open()
{
    m_socket->connectToHost(m_remote_address, m_remote_port);
}

void QTcpConnection::close()
{
    m_socket->disconnectFromHost();
}

void QTcpConnection::send(std::string data)
{
    m_socket->write(QByteArray::fromStdString(data));
}


// Slots

void QTcpConnection::socketConnected()
{
    if(m_listener)
        m_listener->onOpened();
}

void QTcpConnection::socketDisconnected()
{
    if(m_listener)
        m_listener->onClosed();
}

void QTcpConnection::socketReadyRead()
{
    if(m_listener) {
        QByteArray data = m_socket->readAll();
        m_listener->onReceived(data.toStdString());
    }
}

