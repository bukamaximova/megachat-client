#include "serverconnection.h"



ServerConnection::ServerConnection(IServerConnectionListener *listener)
{
    m_listener = listener;
    m_is_opened = false;
    m_tcp_connection = 0;
    m_chat_connection = 0;
}


ServerConnection::~ServerConnection()
{
    if(m_tcp_connection != 0) {
        delete m_chat_connection;
        delete m_tcp_connection;
    }
}


void ServerConnection::connectToServer(const QHostAddress &server_address, qint16 port)
{
    m_tcp_connection = new QTcpConnection(server_address, port);
    m_chat_connection = new ChatConnection(m_tcp_connection);
    m_chat_connection->setListener(this);
    m_tcp_connection->setListener(m_chat_connection);
    m_chat_connection->open();
//    m_tcp_connection->open();

    m_is_opened = true;
}

void ServerConnection::disconnectFromServer()
{
    if(m_is_opened)
        m_chat_connection->close();

    m_is_opened = false;
}


void ServerConnection::login(const std::string &username, const std::string &password)
{
    if(!m_is_opened)
        return;

    m_chat_connection->sendLogin(username, password);
}



///////////////////////////
/// Chat events
///////////////////////////
void ServerConnection::onOpened()
{
    qDebug() << "SC: onConnected";
    m_listener->onConnected();
//    emit onConnected();
}

void ServerConnection::onClosed()
{
    qDebug() << "SC: onDisconnected";
    m_listener->onDisconnected();
}

void ServerConnection::onLoginAck(char code)
{
    m_listener->onLoginAck(code);
}


void ServerConnection::onLogoutAck()
{
    m_listener->onLogoutAck();
}


void ServerConnection::onMessageOutAck(int message_id, char code)
{
    if(code)
        m_listener->onMessageDelivered(message_id, code == 0);
}


void ServerConnection::onMessageIn(std::string sender, int message_id, std::string message)
{
    m_listener->onMessageIncoming(sender, message_id, message);
}

void ServerConnection::onPresence(std::string username, u_int8_t status)
{
    m_listener->onContactStatusChanged(username,status);
}

