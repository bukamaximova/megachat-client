#ifndef MAINWINDOWCONTROLLER_H
#define MAINWINDOWCONTROLLER_H


#include <QMap>
#include <string>

#include "mainwindowmodel.h"
#include "mainwindow.h"

#include <structuser.h>



class MainWindowController
{
private:
    MainWindow      m_window;
    MainWindowModel m_model;

public:
    MainWindowController();

    void showWindow();
    void hideWindow();

    void setContactList(const std::vector<User>& contacts);
    void statusChanged(const std::string &username, char status);

//signals:
//    void
//public slots:
//    void
};

#endif // MAINWINDOWCONTROLLER_H
