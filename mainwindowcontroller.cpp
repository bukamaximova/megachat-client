#include "mainwindowcontroller.h"

MainWindowController::MainWindowController()
{
//    QObject::connect(&window, SIGNAL(signalLogin(std::string,std::string)),
//                     &model, SLOT(slotLogin(std::string,std::string)));
//    window.show();
    QObject::connect(&m_model,SIGNAL(signalUpdateContactList(const QMap<std::string,char>*)),
                     &m_window, SLOT(slotUpdateContactList(const QMap<std::string,char>*)));

}



void MainWindowController::hideWindow()
{
    m_window.hide();
}



void MainWindowController::showWindow()
{
    m_window.show();
}



void MainWindowController::setContactList(const std::vector<User> &contacts)
{
    for(int i = 0; i < contacts.size(); i++){
        User contact = contacts[i];
        m_model.addContact(contact.username, contact.status);
    }
}

void MainWindowController::statusChanged(const std::string &username, char status)
{
    //    -> m_mainWindow_Controller::statusChanged(status) -> model::updateStatus() -> signal to Window
    m_model.updateStatus(username, status);
}
