#include "loginwindow.h"
#include "ui_loginwindow.h"

#include <QtGui>
#include <QtWidgets>

#include "qtcpconnection.h"

QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
// You may want to use QRegularExpression for new code with Qt 5 (not mandatory).
QRegExp ipRegex ("^" + ipRange
                 + "\\." + ipRange
                 + "\\." + ipRange
                 + "\\." + ipRange + "$");


LoginWindow::LoginWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LoginWindow)
{
    ui->setupUi(this);

    setFixedSize(290, 160);

    //range of available symbols for login and password
    QString symbolRange = "[a-zA-Z0-9\.+-]{1,32}";
    QRegExpValidator *symbolValidator = new QRegExpValidator(QRegExp(symbolRange),this);
    QRegExpValidator *ipValidator = new QRegExpValidator(ipRegex, this);
    // 0  <= 65535
    QString numRange = "[1-9]{1}[0-9]{1,4}";
    QRegExpValidator *portValidator = new QRegExpValidator(QRegExp(numRange),this);
    ui->portEdit->setValidator(portValidator);

    ui->ipEdit->setValidator(ipValidator);
    ui->userEdit->setValidator(symbolValidator);
    ui->userEdit->setMaxLength(32);
    ui->passwordEdit->setValidator(symbolValidator);
    ui->passwordEdit->setMaxLength(32);
    ui->passwordEdit->setEchoMode(QLineEdit::Password);

    connect(ui->quitButton,SIGNAL(clicked()),this,SLOT(close()));
    connect(ui->loginButton, SIGNAL(clicked()), this, SLOT(slotLoginClicked()));
}


LoginWindow::~LoginWindow()
{
    delete ui;
}


void LoginWindow::showMessageLoginFailed()
{
    qDebug() << "Show login failed message box.";
    QMessageBox * error_popup = new QMessageBox(this);
    error_popup->setText("Incorrect login or password.");
    error_popup->show();
    connect(error_popup, &QMessageBox::accepted, error_popup, &QMessageBox::deleteLater);
    connect(error_popup, &QMessageBox::rejected, error_popup, &QMessageBox::deleteLater);
}


void LoginWindow::slotLoginClicked()
{
    qDebug() << "Login clicked";

    QRegExpValidator *ipValidator = new QRegExpValidator(ipRegex, this);

    qDebug() << ipRegex.exactMatch(ui->ipEdit->text());
    if (ipRegex.exactMatch(ui->ipEdit->text()) && (ui->portEdit->text().length() > 0) && (ui->portEdit->text().toInt()<=65535))
    {
            emit signalLogin(QHostAddress(ui->ipEdit->text()),
                             ui->portEdit->text().toInt(),
                             ui->userEdit->text().toStdString(),
                             ui->passwordEdit->text().toStdString());
    }
    else
    {
        qDebug() << "Show connect failed message box.";
        QMessageBox * error_popup = new QMessageBox(this);
        error_popup->setText("Incorrect server address.");
        error_popup->show();
        connect(error_popup, &QMessageBox::accepted, error_popup, &QMessageBox::deleteLater);
        connect(error_popup, &QMessageBox::rejected, error_popup, &QMessageBox::deleteLater);
    }

}



