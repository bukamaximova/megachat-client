#ifndef CHATWINDOWMODEL_H
#define CHATWINDOWMODEL_H

#include <string>
#include <vector>
#include <QObject>

struct s_History
{
    bool isSender;
    std::string message;
    s_History(bool _isSender, std::string _message)
    {
        isSender = _isSender;
        message = _message;
    }
};

class ChatWindowModel: public QObject
{
    Q_OBJECT
public:
    ChatWindowModel(QObject *parent = 0);
    ~ChatWindowModel();
private:
    std::string m_message;
    std::vector <s_History> m_history;
    std::string m_receiver;
public slots:
    void slotSendMessage(std::string _message);
};

#endif // CHATWINDOWMODEL_H
