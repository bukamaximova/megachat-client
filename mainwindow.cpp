#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include <QtWidgets>
#include <QtCore>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QListWidgetItem* item = new QListWidgetItem(QIcon("media/off.png"),QString("Petuh"));
    ui->listWidget->addItem(item);

    for (unsigned short int i = 1; i < 30; i++)
    {
        item = new QListWidgetItem(QIcon(QPixmap("media/on.png")),QString("Petuh"+QString::number(i)));
        ui->listWidget->addItem(item);
    }

    connect(ui->listWidget,SIGNAL(doubleClicked(QModelIndex)),
            this,SLOT(slotDoubleClicked(QModelIndex)));

    connect(ui->opedDialogButton, SIGNAL(clicked(bool)),
            this, SLOT(openDialogClicked(bool)));

    connect(ui->listWidget, &QListWidget::itemSelectionChanged,
            [&](){
        ui->opedDialogButton->setEnabled(true);
    });
}


void MainWindow::slotDoubleClicked(QModelIndex index)
{
    openDialog(index.data(0).toString());
}

void MainWindow::slotUpdateContactList(const QMap<std::string, char> * contact_list)
{
    ui->listWidget->clear();
    QListWidgetItem* item;
    for (auto it = contact_list->begin(); it != contact_list->end(); it++)
    {
        if (it.value() == 0)
            item =  new QListWidgetItem(QIcon(QPixmap("media/on.png")),QString::fromStdString(it.key()));
        else
            item =  new QListWidgetItem(QIcon(QPixmap("media/off.png")),QString::fromStdString(it.key()));

        ui->listWidget->addItem(item);
    }
}

void MainWindow::openDialogClicked(bool)
{
    openDialog(ui->listWidget->selectedItems()[0]->text());
}


void MainWindow::openDialog(const QString &username){
    qDebug() << "Open dialog with " << username;
}



//slotModelUdated
/*
ui->listWidget->clear
model.getContacts()
foreach
{
    QString staus = (contacts[i].status == 1) ? "[ON]" : "[OFF]";
    item = new QListWidgetItem(QIcon("media/user.png"), status + username);
    ui->listWidget->addItem(item);
}


*/
