#ifndef MAINWINDOWMODEL_H
#define MAINWINDOWMODEL_H

#include <QObject>
#include <QMap>


class MainWindowModel:public QObject
{
    Q_OBJECT
private:
    QMap<std::string, char> m_contacts;

public:
    MainWindowModel(QObject *parent = 0);
    ~MainWindowModel();

    void clear();
    void addContact(std::string username, char status);
    void updateStatus(std::string username, char status);
    const QMap<std::string, char> * getContacts() const;

signals:
    void signalUpdateContactList(const QMap<std::string, char> *);

private:



};

#endif // MAINWINDOWMODEL_H
