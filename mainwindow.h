#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QModelIndex>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);

signals:

private slots:
    void openDialog(const QString& username);
    void slotUpdateContactList(const QMap<std::string, char> *contact_list);
    void slotDoubleClicked(QModelIndex index);
    void openDialogClicked(bool);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
