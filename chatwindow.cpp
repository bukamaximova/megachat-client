#include "chatwindow.h"
#include "ui_chatwindow.h"

#include <QtGui>
#include <QtWidgets>
#include <QDebug>
#include <QTextEdit>
ChatWindow::ChatWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ChatWindow)
{
    ui->setupUi(this);

    m_messageLength = 0;
    QHBoxLayout* hbox = new QHBoxLayout();
//    ui->messageEdit->setPlaceholderText("Outgoing message");
    hbox->addWidget(ui->messageEdit);
    ui->sendButton->setDisabled(true);
    hbox->addWidget(ui->sendButton);
    QVBoxLayout* vbox = new QVBoxLayout();
    ui->dialogHistory->setReadOnly(true);
    vbox->addWidget(ui->dialogHistory);
    vbox->addLayout(hbox);
    ui->centralwidget->setLayout(vbox);

    connect(ui->messageEdit,SIGNAL(textChanged()), this, SLOT(slotCheckLength()));
    connect(ui->sendButton, SIGNAL(clicked()), this, SLOT(slotSendButtonClicked()));
}

void ChatWindow::slotCheckLength()
{
    m_messageLength = ui->messageEdit->toPlainText().length();
    if (m_messageLength < 1)
        ui->sendButton->setDisabled(true);
    else
        ui->sendButton->setEnabled(true);
    if (m_messageLength > 256)
        ui->messageEdit->textCursor().deletePreviousChar();
    qDebug()<<"slot check length";
}

void ChatWindow::slotSendButtonClicked()
{
    emit signalSendMessage(ui->messageEdit->toPlainText().toStdString());
    ui->dialogHistory->append(QString("me: "+ui->messageEdit->toPlainText()));
    ui->messageEdit->clear();
}

void ChatWindow::keyPressEvent(QKeyEvent * event)
{
//    if (event->key() == Qt::Key_Enter)
//    {
//        emit ui->sendButton->clicked();
//    }
}
