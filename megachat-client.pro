#-------------------------------------------------
#
# Project created by QtCreator 2016-06-12T13:31:30
#
#-------------------------------------------------

QT       += core gui network
CONFIG += c++11


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = megachat-client
TEMPLATE = app

INCLUDEPATH += ../megachat-client/include
LIBS += -L../megachat-client -lmegachat-networklib

SOURCES += main.cpp\
    loginwindow.cpp \
    loginwindowcontroller.cpp \
    loginwindowmodel.cpp \
    client.cpp \
    mainwindow.cpp \
    mainwindowmodel.cpp \
    mainwindowcontroller.cpp \
    chatwindowcontroller.cpp \
    chatwindowmodel.cpp \
    chatwindow.cpp \
    chatwindowlist.cpp \
    serverconnection.cpp \
    qtcpconnection.cpp

HEADERS  += loginwindow.h \
    loginwindowcontroller.h \
    loginwindowmodel.h \
    client.h \
    mainwindow.h \
    mainwindowmodel.h \
    mainwindowcontroller.h \
    chatwindowcontroller.h \
    chatwindowmodel.h \
    chatwindow.h \
    chatwindowlist.h \
    serverconnection.h \
    qtcpconnection.h

FORMS    += loginwindow.ui \
    mainwindow.ui \
    chatwindow.ui

