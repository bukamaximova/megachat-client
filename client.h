#ifndef CLIENT_H
#define CLIENT_H

#include <string>

#include <QApplication>

// Library
#include <eventmanager.h>

// Client includes
#include "serverconnection.h"
#include "loginwindowcontroller.h"
#include "chatwindowlist.h"
#include "mainwindowcontroller.h"



class Client : public IServerConnectionListener
{
private:
    std::string             m_username;

//    EventManager *          m_eventManager;
    QApplication *          m_qtApplication;

    ServerConnection *      m_serverConnection; ///< Connection to server

    // Window controllers
    LoginWindowController * m_loginWindowController;
    MainWindowController *  m_mainWindow_Controller;
    ChatWindowList *        m_chatWindowList;

public:
    Client(QApplication * app);
    virtual ~Client();

    void launch();
    void quit();

    void onConnected();
    void onDisconnected();
    void onLoginAck(char code);
    void onLogoutAck();


    void onMessageIncoming(const std::string &sender, int message_id, const std::string &message);
    void onMessageDelivered(int message_id, bool success);
    void onContactStatusChanged(const std::string &username, char status);

};

//[ON OFF] pehuh
// x o

#endif // CLIENT_H
