#ifndef LOGINWINDOWCONTROLLER_H
#define LOGINWINDOWCONTROLLER_H

#include "loginwindow.h"
#include "loginwindowmodel.h"
#include "serverconnection.h"

class LoginWindowController : public QObject //: public ILoginWindowListener
{
    Q_OBJECT
private:
    bool                m_hidden;

    std::string         m_login;
    std::string         m_password;
    QHostAddress        m_server_address;

    LoginWindow         m_window;
    LoginWindowModel    m_model;
    ServerConnection *  m_connection;

public:
    LoginWindowController(ServerConnection * connection);
    ~LoginWindowController() {}

    void showWindow();
    void hideWindow();

    void showLoginFailed();

    void connectedToServer();


public slots:
    // Events from window
    void onLoginClicked(QHostAddress server, qint16 port, std::string login, std::string password);

};

#endif // LOGINWINDOWCONTROLLER_H
