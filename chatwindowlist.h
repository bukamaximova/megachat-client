#ifndef CHATWINDOWLIST_H
#define CHATWINDOWLIST_H

#include <string>
#include <vector>
#include "chatwindowcontroller.h"

class ChatWindowList
{
private:
    std::vector <ChatWindowController *> list;

public:
    ChatWindowList();

    void displayMessage(const std::string& sender, int message_id, const std::string& message);
    void messageDelivered(int message_id, bool success);
};

#endif // CHATWINDOWLIST_H
