#ifndef QTCPCONNECTION_H
#define QTCPCONNECTION_H

#include <QObject>
#include <QtNetwork>

#include <interfaces/ibyteconnection.h>
#include <interfaces/ibyteconnectionlistener.h>


class QTcpConnection : public QObject, public IByteConnection
{
    Q_OBJECT
private:
    QTcpSocket * m_socket;
    IByteConnectionListener * m_listener;
    QHostAddress m_remote_address;
    qint16 m_remote_port;

public:
    explicit QTcpConnection(const QHostAddress& address, qint16 port, QObject *parent = 0);
    virtual ~QTcpConnection();
    void setListener(IByteConnectionListener * listener);

    virtual void open();
    virtual void close();
    virtual void send(std::string data);

signals:

public slots:

private slots:
    void socketConnected();
    void socketDisconnected();
    void socketReadyRead();
};

#endif // QTCPCONNECTION_H
