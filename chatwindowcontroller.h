#ifndef CHATWINDOWCONTROLLER_H
#define CHATWINDOWCONTROLLER_H


#include "chatwindow.h"
#include "chatwindowmodel.h"

class ChatWindowController
{
public:
    ChatWindowController();

private:
    ChatWindow window;
    ChatWindowModel model;
};

#endif // CHATWINDOWCONTROLLER_H
